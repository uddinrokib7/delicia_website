// Toggle advanced options
$(document).ready(function(){
    $("#loc-adv a").click(function(){
        $("#map").slideToggle("slow");
        return false;
    });
});

// Add .last class to certain lists
$(document).ready(function(){
    $("ul.lst li:last, ul.press li:last").addClass("last");
});

// Slide down map when enter pressed in Location input field
$(document).ready(function(){
    $("input#loc-text").keydown(function(e){
        if (e.keyCode === 13) {
            e.preventDefault();
            $("#map").slideDown("slow");
            return false;
        }
    });
});
